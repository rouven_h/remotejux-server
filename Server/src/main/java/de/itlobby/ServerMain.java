package de.itlobby;

import de.itlobby.settings.Settings;
import de.itlobby.ws.ServerResource;
import org.apache.log4j.BasicConfigurator;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.simple.SimpleContainerFactory;
import org.glassfish.jersey.simple.SimpleServer;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class ServerMain
{
    public static void main(String[] args)
    {
        try
        {
            BasicConfigurator.configure();
            initializeWebServer();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void initializeWebServer()
    {
        String serverName = Settings.getInstance().getConfig().getServerName();
        Integer port = Integer.valueOf(Settings.getInstance().getConfig().getServerPort());

        URI baseUri = UriBuilder.fromUri("http://" + serverName + "/").port(port).build();

        ResourceConfig config = new ResourceConfig(
            ServerResource.class
        );

        SimpleServer server = SimpleContainerFactory.create(baseUri, config);
        System.out.println("Started server @ " + baseUri);
    }
}
