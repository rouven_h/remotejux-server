package de.itlobby.ws;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.CLICommand;
import de.itlobby.models.CLIResponse;
import de.itlobby.services.SSHService;
import de.itlobby.utils.MapUtil;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("cmd")
public class ServerResource
{
    private final SSHService sshService;

    public ServerResource()
    {
        sshService = ServiceLocator.getServiceInstance(SSHService.class);
    }

    @GET
    @Path("ping")
    @Produces(MediaType.APPLICATION_JSON)
    public String ping()
    {
        long ping = System.currentTimeMillis();
        System.out.println("Ping" + ping);
        return MapUtil.toJSON(ping);
    }

    @POST
    @Path("execute")
    @Consumes(MediaType.APPLICATION_JSON)
    public String executeCommand(String command)
    {
        CLICommand cliCommand = MapUtil.toObject(command, CLICommand.class);
        System.out.println("executing: " + cliCommand.getCommand());

        CLIResponse response = sshService.executeCommandAsCLI(cliCommand);

        System.out.println("response: " + response.getResult());

        return MapUtil.toJSON(response);
    }
}
