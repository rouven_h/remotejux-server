package de.itlobby.services;

import de.itlobby.models.CLICommand;
import de.itlobby.models.CLIResponse;
import de.itlobby.utils.CommandHelper;

public class SSHService
{
    public CLIResponse executeCommandAsCLI(CLICommand command)
    {
        System.out.println("executing Command " + command.getCommand());

        CLIResponse cliResponse = new CLIResponse();

        String stringResponse = CommandHelper.executeCommand(command.getCommand());

        cliResponse.setResult(stringResponse);

        return cliResponse;
    }
}
