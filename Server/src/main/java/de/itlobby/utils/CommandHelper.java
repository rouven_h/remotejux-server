package de.itlobby.utils;

import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CommandHelper
{
    protected static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CommandHelper.class);

    public static String executeCommand(String fullCommand)
    {
        LOG.info("Executing command: " + fullCommand);

        List<String> command = parseCommand(fullCommand);

        String result = execute(command);

        LOG.info("Result: " + result);

        return result;
    }

    private static List<String> parseCommand(String fullCommand)
    {
        String[] split = fullCommand.split(" ");
        return Arrays.asList(split);
    }

    private static String execute(List<String> command)
    {
        try
        {
            File tempFile = File.createTempFile("tmp", null);

            ProcessBuilder pb = new ProcessBuilder(command);
            pb.redirectError(tempFile);
            pb.redirectOutput(tempFile);
            Process process = pb.start();

            process.waitFor();

            return getContent(tempFile);
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
            return e.getMessage();
        }
    }

    private static String getContent(File tempFile) throws IOException
    {
        String out = "";

        BufferedReader reader = new BufferedReader(new FileReader(tempFile));
        String line;
        while ((line = reader.readLine()) != null)
        {
            out += line + "\n";
        }

        if (tempFile.delete())
        {
            LOG.info("tempfile sucessfull deleted");
        }
        else
        {
            LOG.warn("cannot delete tempfile " + tempFile.getName());
        }

        return out;
    }
}
