package de.itlobby.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil
{
    public static boolean isNullOrEmpty(String str)
    {
        return str == null || str.trim().equals("");
    }

    public static String getStringSeq(String query, int start, int end)
    {
        return query.substring(start, end);
    }

    static String removeBetween(String text, String start, String end)
    {
        int first = text.indexOf(start);
        int last = text.indexOf(end) + 1;
        int diff = last - first;

        if (diff > 0)
        {
            String seq = getStringSeq(text, first, last);
            text = text.replace(seq, "");
        }

        return text;
    }

    public static String getBetween(String text, String start, String end)
    {
        int first = text.indexOf(start) + start.length();
        int last = text.indexOf(end);
        int diff = last - first;

        if (diff > 0)
        {
            return getStringSeq(text, first, last);
        }

        return text;
    }

    public static String dateToString(Date date)
    {
        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }
}
