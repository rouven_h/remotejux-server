/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package de.itlobby.framework;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LOD
 *         <p>
 *         some utils fpr the dtos
 */
public class DTOUtil
{

    private static Mapper MAPPER;

    private static Mapper getMapper()
    {
        if (MAPPER == null)
        {
            MAPPER = new DozerBeanMapper();
        }
        return MAPPER;
    }

    /**
     * build a copy of the given source list
     * for the type of a aClass
     *
     * @param aClass  a class
     * @param sources a list of sources
     * @return list of objects containing the propertys of the source
     */
    public static <T> List<T> buildAll(List sources, Class<? extends T> aClass)
    {
        final List<T> dtos = new ArrayList<T>(sources.size());
        for (Object source : sources)
        {
            dtos.add(build(source, aClass));
        }
        return dtos;
    }

    /**
     * build a copy of the given source list
     * for the type of a aClass
     *
     * @param aClass a class
     * @param source a source
     * @return list of objects containing the propertys of the source
     */
    public static <T> T build(Object source, Class<? extends T> aCass)
    {
        if (source == null)
        {
            return null;
        }
        return getMapper().map(source, aCass);
    }

    /**
     * copys the proeprties from the source to the target object (deepcopy)
     *
     * @param source a class
     * @param target a source
     */
    public static void copy(Object source, Object target)
    {
        getMapper().map(source, target);
    }
}
