package de.itlobby.framework;

import de.itlobby.viewcontroller.MainViewController;
import de.itlobby.viewcontroller.ViewController;

public enum Views
{
    MainView("views/MainView.fxml", "remoteJux", MainViewController.class);

    private final String path;
    private final Class<ViewController> clazz;
    private String title;

    Views(String path, String title, Class<?> clazz)
    {
        this.path = path;
        this.title = title;
        this.clazz = (Class<ViewController>) clazz;
    }

    public String getPath()
    {
        return path;
    }

    public String getTitle()
    {
        return title;
    }

    public Class<ViewController> getClazz()
    {
        return clazz;
    }
}
