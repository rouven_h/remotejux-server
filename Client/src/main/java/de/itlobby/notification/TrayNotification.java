package de.itlobby.notification;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class TrayNotification
{
    public static void show(String title, String message, Image icon, Stage ownerStage)
    {
        Scene scene = new Scene(createLayout(title, message, icon));
        _show(ownerStage, scene);
    }

    public static void show(String title, String message, Stage ownerStage)
    {
        Scene scene = new Scene(createLayout(title, message));
        _show(ownerStage, scene);
    }

    private static void _show(Stage owner, Scene scene)
    {
        scene.getStylesheets().add("css/NotificationStyle.css");
        scene.setFill(null);

        Stage stage = new Stage();
        //stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setScene(scene);
        stage.initOwner(owner);

        stage.setHeight(100);
        stage.setWidth(400);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double finalX = screenSize.getWidth() - (stage.getWidth() + 5);
        double finalY = screenSize.getHeight() - (stage.getHeight() + 50);

        double startY = screenSize.getHeight() + stage.getHeight();

        stage.setX(finalX);
        stage.setY(startY);

        stage.show();

        double diff = startY - finalY;

        showAnimatedNotification(stage, diff);
    }

    private static void showAnimatedNotification(Stage stage, double diff)
    {
        new Thread(
            () ->
            {
                for (int i = 0; i < diff; i++)
                {
                    Platform.runLater(
                        () -> stage.setY(stage.getY() - 1)
                    );

                    sleep(2);
                }

                sleep(5000);

                for (int i = 0; i < diff; i++)
                {
                    Platform.runLater(
                        () -> stage.setY(stage.getY() + 1)
                    );

                    sleep(2);
                }

                Platform.runLater(stage::close);
            }
        ).start();
    }

    private static void sleep(int millis)
    {
        try
        {
            Thread.sleep(millis);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private static Parent createLayout(String title, String message)
    {
        TextFlow txtTitle = new TextFlow(new Text(title));
        txtTitle.setTextAlignment(TextAlignment.CENTER);
        txtTitle.getStyleClass().add("title");
        txtTitle.setPrefHeight(25);
        txtTitle.setPadding(new Insets(5, 0, 5, 0));

        TextFlow txtMessage = new TextFlow(new Text(message));
        txtMessage.setPadding(new Insets(5));

        VBox vBox = new VBox();
        vBox.getStyleClass().add("notification_style");

        vBox.getChildren().add(txtTitle);
        vBox.getChildren().add(txtMessage);

        return vBox;
    }

    private static Parent createLayout(String title, String message, Image icon)
    {
        TextFlow txtTitle = new TextFlow(new Text(title));
        txtTitle.setTextAlignment(TextAlignment.CENTER);
        txtTitle.getStyleClass().add("title");
        txtTitle.setPrefHeight(25);
        txtTitle.setPadding(new Insets(5, 0, 5, 0));

        TextFlow txtMessage = new TextFlow(new Text(message));
        txtMessage.setPadding(new Insets(5));

        VBox vBox = new VBox();
        vBox.getStyleClass().add("notification_style");

        HBox hBox = new HBox();
        ImageView iconImage = new ImageView(icon);
        HBox.setMargin(iconImage, new Insets(5));
        hBox.getChildren().add(iconImage);
        hBox.getChildren().add(txtMessage);

        vBox.getChildren().add(txtTitle);
        vBox.getChildren().add(hBox);

        return vBox;
    }
}
