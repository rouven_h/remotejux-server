package de.itlobby.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionUtil
{
    private static Logger LOG = Logger.getLogger(ExceptionUtil.class.getName());

    public static void logException(Exception e)
    {
        LOG.log(Level.SEVERE, e.getMessage(), e);
        e.printStackTrace();
        if (e.getCause() != null)
        {
            e.getCause().printStackTrace();
        }
    }
}
