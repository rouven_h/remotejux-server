package de.itlobby.util;

import javafx.application.Platform;
import javafx.css.Styleable;
import javafx.scene.Node;

import java.net.URL;

public class SystemUtil
{
    public static URL getResourceURL(String path)
    {
        return Thread.currentThread().getContextClassLoader().getResource(path);
    }

    public static void setOnlyCSSClass(Styleable node, String styleClass)
    {
        node.getStyleClass().clear();
        node.getStyleClass().add(styleClass);
    }

    public static void addCSSClass(Styleable node, String... styleClasses)
    {
        for (String styleClass : styleClasses)
        {
            node.getStyleClass().add(styleClass);
        }
    }

    public static void focusNode(Node node)
    {
        Platform.runLater(node::requestFocus);
    }
}
