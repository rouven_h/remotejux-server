package de.itlobby.models;

import java.io.Serializable;

public class CLICommand implements Serializable
{
    private String command;
    private boolean requiresInput;
    private boolean returnsText;

    public CLICommand()
    {
    }

    public CLICommand(String command, Boolean requiresInput, Boolean returnsText)
    {
        this.command = command;
        this.requiresInput = requiresInput;
        this.returnsText = returnsText;

        if (requiresInput)
        {
            this.returnsText = true;
        }
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    public boolean isRequiresInput()
    {
        return requiresInput;
    }

    public void setRequiresInput(boolean requiresInput)
    {
        this.requiresInput = requiresInput;
    }

    public boolean isReturnsText()
    {
        return returnsText;
    }

    public void setReturnsText(boolean returnsText)
    {
        this.returnsText = returnsText;
    }
}
