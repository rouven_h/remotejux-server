package de.itlobby.services;

import de.itlobby.framework.ViewManager;
import de.itlobby.framework.Views;
import de.itlobby.viewcontroller.MainViewController;

public abstract class AbstractService
{
    MainViewController mainViewController;

    public AbstractService()
    {
        mainViewController = ViewManager.getInstance().getViewController(Views.MainView);
    }
}
