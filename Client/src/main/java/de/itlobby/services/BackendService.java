package de.itlobby.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import de.itlobby.models.CLICommand;
import de.itlobby.models.CLIResponse;
import de.itlobby.settings.Settings;
import de.itlobby.util.MapUtil;

import javax.ws.rs.core.MediaType;
import java.net.URL;

public class BackendService
{
    private static BackendService instance;
    private final String servername;
    private final String port;
    private boolean isOnline;

    private BackendService()
    {
        Settings settings = Settings.getInstance();

        servername = settings.getConfig().getServerName();
        port = settings.getConfig().getServerPort();
    }

    public static BackendService getInstance()
    {
        if (instance == null)
        {
            instance = new BackendService();
        }

        return instance;
    }

    private String getBaseUrl()
    {
        return "http://" + servername + ":" + port + "/cmd/";
    }

    public boolean checkWebService()
    {
        try
        {
            new URL(getBaseUrl() + "ping").openStream();
            isOnline = true;
        }
        catch (Exception e)
        {
            isOnline = false;
        }

        return isOnline;
    }

    private <T> T restPOST(Object obj, String method, Class<T> returnType)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl() + method);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, MapUtil.toJSON(obj));
        return MapUtil.toObject(response.getEntity(String.class), returnType);
    }

    private <T> T restPOSTArray(String objArray, String method, Class<T> targetClass)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl() + method);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, MapUtil.toJSON(objArray));
        return MapUtil.toObject(response.getEntity(String.class), targetClass);
    }

    private boolean restDELETE(String method)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl() + method);
        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        return MapUtil.toObject(response.getEntity(String.class), Boolean.class);
    }

    private <T> T restGET(Class<T> targetClass, String wsMethodName)
    {
        String jsonContent = MapUtil.readJsonContentFromUrl(getBaseUrl(), wsMethodName);
        return MapUtil.toObject(jsonContent, targetClass);
    }

    public boolean isOnline()
    {
        return isOnline;
    }

    public CLIResponse executeCommand(CLICommand command)
    {
        return restPOST(command, "execute", CLIResponse.class);
    }
}
