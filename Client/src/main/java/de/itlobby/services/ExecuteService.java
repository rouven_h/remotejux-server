package de.itlobby.services;

import de.itlobby.controls.CommandButton;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.CLICommand;
import de.itlobby.models.CLIResponse;
import de.itlobby.util.StringUtil;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ExecuteService extends AbstractService
{
    public void onCreateNewCommandButton()
    {
        LightBoxService lightbox = ServiceLocator.getServiceInstance(LightBoxService.class);
        GridPane form = new GridPane();
        form.setVgap(10);
        form.setHgap(5);

        TextField txtCommandLabel = new TextField();
        form.add(new Label("Name des Befehls:"), 0, 0);
        form.add(txtCommandLabel, 1, 0);

        TextField txtCommand = new TextField();
        form.add(new Label("Befehl:"), 0, 1);
        form.add(txtCommand, 1, 1);

        CheckBox chkReturnsText = new CheckBox("Befehl gibt Text zurück");
        form.add(chkReturnsText, 0, 2, 2, 1);

        CheckBox chkUserInput = new CheckBox("Befehl erwartet Benutzereingabe");
        chkUserInput.selectedProperty().addListener((observable, oldValue, newValue) -> userInputCheckBoxChanged(chkReturnsText, newValue));
        form.add(chkUserInput, 0, 3, 2, 1);

        lightbox.showDialog("Neuen Befehl anlegen", form, lightbox::hideDialog, () -> createNewCommandButton(lightbox, txtCommandLabel, txtCommand, chkUserInput, chkReturnsText));
    }

    private void userInputCheckBoxChanged(CheckBox chkReturnsText, Boolean newValue)
    {
        if (newValue)
        {
            chkReturnsText.setSelected(true);
            chkReturnsText.setDisable(true);
        }
        else
        {
            chkReturnsText.setDisable(false);
        }
    }

    private void createNewCommandButton(LightBoxService lightbox, TextField txtCommandLabel, TextField txtCommand, CheckBox chkUserInput, CheckBox chkReturnsText)
    {
        CLICommand cliCommand = new CLICommand(txtCommand.getText(), chkUserInput.isSelected(), chkReturnsText.isSelected());
        CommandButton commandButton = new CommandButton(txtCommandLabel.getText(), cliCommand);

        mainViewController.fpButtonFlow.getChildren().add(0, commandButton);

        lightbox.hideDialog();
    }

    public void onExecuteCommand(CommandButton cliCommandButton)
    {
        CLICommand cliCommand = cliCommandButton.getCommand();

        if (cliCommand != null && !cliCommand.getCommand().equals(""))
        {
            CLIResponse cliResponse = BackendService.getInstance().executeCommand(cliCommand);

            if (cliCommand.isReturnsText() && !StringUtil.isNullOrEmpty(cliResponse.getResult()))
            {
                LightBoxService lightbox = ServiceLocator.getServiceInstance(LightBoxService.class);
                TextArea textArea = new TextArea();
                textArea.setText(cliResponse.getResult());
                textArea.setEditable(false);

                lightbox.showDialog("Rückgabe von " + cliCommandButton.getText(), textArea, lightbox::hideDialog, lightbox::hideDialog, true);
            }
        }
    }
}
