package de.itlobby.services;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.util.SystemUtil;
import javafx.scene.control.Button;

public class MainService extends AbstractService
{
    public MainService()
    {
        initialize();
    }

    private void initialize()
    {
        checkServerConnection();

        Button addNewButton = new Button("+");
        SystemUtil.setOnlyCSSClass(addNewButton, "cmd_button_add");
        addNewButton.setOnAction(event -> ServiceLocator.getServiceInstance(ExecuteService.class).onCreateNewCommandButton());
        mainViewController.fpButtonFlow.getChildren().add(addNewButton);
    }

    private void checkServerConnection()
    {
        BackendService instance = BackendService.getInstance();

        new Thread(
            () ->
            {
                boolean isOnline = instance.checkWebService();

                if (isOnline)
                {
                    System.out.println("Server is online");
                }
                else
                {
                    System.out.println("Server is offline");
                }
            }
        ).start();
    }

    public void beforeShutdown()
    {

    }
}
