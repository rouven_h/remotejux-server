package de.itlobby.services;

import de.itlobby.listener.ActionListener;
import de.itlobby.util.SystemUtil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class LightBoxService extends AbstractService
{
    public void showDialog(String title, Parent content, ActionListener cancelListener, ActionListener okListener, boolean isFullscreen)
    {
        _show(title, content, cancelListener, okListener, isFullscreen);
    }

    public void showDialog(String title, Parent content, ActionListener cancelListener, ActionListener okListener)
    {
        _show(title, content, cancelListener, okListener, false);
    }

    private void _show(String title, Node content, ActionListener cancelListener, ActionListener okListener, boolean isFullscreen)
    {
        HBox buttonLayout = new HBox(10);
        buttonLayout.setAlignment(Pos.CENTER_RIGHT);

        Button cancelButton = new Button();
        cancelButton.setText("Abbrechen");
        cancelButton.setOnAction(event -> cancelListener.onAction());
        cancelButton.setCancelButton(true);
        SystemUtil.addCSSClass(cancelButton, "color_button");

        Button okButton = new Button();
        okButton.setText("Ok");
        okButton.setOnAction(event -> okListener.onAction());
        okButton.setDefaultButton(true);
        SystemUtil.addCSSClass(okButton, "color_button");

        buttonLayout.getChildren().add(cancelButton);
        buttonLayout.getChildren().add(okButton);

        VBox contentRootBox = new VBox(10);
        contentRootBox.setStyle("-fx-background-color: transparent");
        contentRootBox.getChildren().add(content);
        contentRootBox.getChildren().add(buttonLayout);

        VBox.setVgrow(content, Priority.ALWAYS);
        VBox.setVgrow(buttonLayout, Priority.NEVER);

        VBox layoutBox = new VBox(10);
        SystemUtil.addCSSClass(layoutBox, "light_box_bg");
        TextFlow titleText = new TextFlow(new Text(title));
        titleText.setPadding(new Insets(5, 0, 5, 0));

        SystemUtil.addCSSClass(titleText, "light_box_title");
        layoutBox.getChildren().add(titleText);
        layoutBox.getChildren().add(contentRootBox);

        VBox.setVgrow(titleText, Priority.NEVER);
        VBox.setVgrow(contentRootBox, Priority.ALWAYS);

        layoutBox.setVisible(true);

        if (!isFullscreen)
        {
            layoutBox.setMaxHeight(250);
            layoutBox.setMaxWidth(350);
        }

        layoutBox.setPadding(new Insets(10));

        mainViewController.lbBorderPane.setVisible(true);
        mainViewController.lbBorderPane.setCenter(layoutBox);

        BorderPane.setMargin(layoutBox, new Insets(5));
    }

    public void hideDialog()
    {
        mainViewController.lbBorderPane.setVisible(false);
        mainViewController.lbBorderPane.setCenter(null);
    }
}
