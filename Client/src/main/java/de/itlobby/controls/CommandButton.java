package de.itlobby.controls;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.CLICommand;
import de.itlobby.services.ExecuteService;
import de.itlobby.util.SystemUtil;
import javafx.geometry.Insets;
import javafx.scene.control.Button;

public class CommandButton extends Button
{
    private CLICommand cliCommand;

    public CommandButton(String labelText, CLICommand cliCommand)
    {
        super();

        this.cliCommand = cliCommand;
        SystemUtil.setOnlyCSSClass(this, "cmd_button");
        setText(labelText);
        setPadding(new Insets(5));
        setWrapText(true);

        ExecuteService executeService = ServiceLocator.getServiceInstance(ExecuteService.class);

        setOnAction(event -> executeService.onExecuteCommand(this));
    }

    public CLICommand getCommand()
    {
        return cliCommand;
    }
}
