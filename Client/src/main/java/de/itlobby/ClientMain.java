package de.itlobby;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.framework.ViewManager;
import de.itlobby.services.MainService;
import de.itlobby.settings.Settings;
import javafx.application.Application;
import javafx.stage.Stage;

public class ClientMain extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        ViewManager viewManager = ViewManager.getInstance();
        viewManager.setPrimaryStage(primaryStage);
        viewManager.initialize();
        //viewManager.<MainViewController>getViewController(Views.LoginView).txtUsername.requestFocus();

        //Init MainService & Settings
        MainService mainService = ServiceLocator.getServiceInstance(MainService.class);
        Settings.getInstance().getConfig();
        primaryStage.setOnCloseRequest(event -> mainService.beforeShutdown());
        //primaryStage.getIcons().add(new Image("icon/icon.png"));
    }
}
