package de.itlobby.settings;

import de.itlobby.util.ExceptionUtil;
import de.itlobby.util.StringUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;

public class Settings
{
    public static final String PROPERTIES_FILENAME = "remoteJux.properties";
    private static Settings instance;

    private Settings()
    {
    }

    public static Settings getInstance()
    {
        if (instance == null)
        {
            instance = new Settings();
        }

        return instance;
    }

    public AppConfig getConfig()
    {
        return loadConfig();
    }

    private AppConfig loadConfig()
    {
        File propFile = new File(PROPERTIES_FILENAME);

        if (propFile.exists())
        {
            return readProperties(propFile);
        }
        else
        {
            AppConfig defaultProps = new AppConfig();
            saveConfig(defaultProps);
            return defaultProps;
        }
    }

    private AppConfig readProperties(File propFile)
    {
        try
        {
            AppConfig appConfig = new AppConfig();
            BufferedReader reader = new BufferedReader(new FileReader(propFile));
            String line;
            while ((line = reader.readLine()) != null)
            {
                if (!StringUtil.isNullOrEmpty(line) && line.contains("="))
                {
                    String[] split = line.split("=");

                    if (split.length < 2)
                    {
                        continue;
                    }

                    String key = split[0];
                    String value = split[1];

                    Field field = getValidLine(key);
                    if (field != null)
                    {
                        if (field.getType().equals(String.class))
                        {
                            field.set(appConfig, value);
                        }
                        else if (field.getType().equals(Integer.class))
                        {
                            field.set(appConfig, Integer.parseInt(value));
                        }
                        else if (field.getType().equals(Double.class))
                        {
                            field.set(appConfig, Double.parseDouble(value));
                        }
                        else if (field.getType().equals(Boolean.class))
                        {
                            field.set(appConfig, Boolean.parseBoolean((value)));
                        }
                        else
                        {
                            throw new IllegalStateException("Unhandled settings type " + field.getType());
                        }
                    }
                }
            }

            return appConfig;
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    private Field getValidLine(String key)
    {
        for (Field field : AppConfig.class.getDeclaredFields())
        {
            if (field.getName().equalsIgnoreCase(key))
            {
                return field;
            }
        }

        return null;
    }

    public void saveConfig(AppConfig appConfig)
    {
        try
        {
            File propFile = new File(PROPERTIES_FILENAME);
            FileWriter fw = new FileWriter(propFile, false);

            for (Field field : appConfig.getClass().getDeclaredFields())
            {
//                if (field.getType().equals(Locale.class))
//                {
//                    Language language = (Language) field.get(appConfig);
//                    fw.write(field.getName() + "=" + language.toString() + "\n");
//                }
//                else
//                {
                fw.write(field.getName() + "=" + field.get(appConfig) + "\n");
                //}
            }

            fw.flush();
            fw.close();
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }
    }
}
